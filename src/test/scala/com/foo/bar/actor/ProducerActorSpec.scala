package com.foo.bar.actor

import java.util.concurrent.CompletableFuture

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import akka.util.Timeout
import com.foo.bar.model.{KafkaRecord, TopicEvent}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord, RecordMetadata}
import org.apache.kafka.common.TopicPartition
import org.mockito.ArgumentCaptor
import org.mockito.Mockito._
import org.mockito.Matchers._
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpecLike}
import org.scalatest.concurrent.{Eventually, ScalaFutures}
import org.scalatest.mockito.MockitoSugar

import scala.concurrent.duration._

class ProducerActorSpec
    extends TestKit(ActorSystem("system")) with WordSpecLike with ImplicitSender with Matchers with BeforeAndAfterEach with ScalaFutures
    with MockitoSugar with Eventually {

  override implicit val patienceConfig: PatienceConfig = PatienceConfig(5.seconds, 300.millis)
  implicit val timeout: Timeout                        = Timeout(5 seconds)
  val producer                                         = mock[KafkaProducer[String, TopicEvent]]
  val topic                                            = "foo-topic"
  val actor                                            = system.actorOf(Props.create(classOf[ProducerActor[String, TopicEvent]], () => producer, topic), "kafkaProducer")

  val fakeMetadata: RecordMetadata = new RecordMetadata(new TopicPartition(topic, 0), 0, 0, 0, 0L, 0, 0)

  val testProbe = TestProbe()
  testProbe watch actor

  "ProducerActor" should {

    "receive the event message and sent it to Kafka" in {
      val event  = TopicEvent(1L, "foo", 1L, Map())
      val record = new ProducerRecord[String, TopicEvent](topic, "1", event)

      when(producer.send(any())).thenReturn(CompletableFuture.completedFuture(fakeMetadata))

      actor ! KafkaRecord[String, TopicEvent]("1", event)

      val captor = ArgumentCaptor.forClass(classOf[ProducerRecord[String, TopicEvent]])

      testProbe.awaitAssert(verify(producer).send(captor.capture()), 5.seconds)

      captor.getValue.key() shouldBe record.key()
      captor.getValue.value() shouldBe record.value()
      captor.getValue.topic() shouldBe topic

    }

    "resume and send next message if fails" in {
      val event  = TopicEvent(1L, "foo", 1L, Map())
      val record = new ProducerRecord[String, TopicEvent](topic, "1", event)

      when(producer.send(any())).thenThrow(new RuntimeException("BOOOM!"))

      actor ! KafkaRecord[String, TopicEvent]("1", event)

      val captor = ArgumentCaptor.forClass(classOf[ProducerRecord[String, TopicEvent]])
      testProbe.awaitAssert(verify(producer).send(captor.capture()), 5.seconds)

      reset(producer)

      when(producer.send(any())).thenReturn(CompletableFuture.completedFuture(fakeMetadata))

      actor ! KafkaRecord[String, TopicEvent]("1", event)
      testProbe.awaitAssert(verify(producer).send(captor.capture()), 5.seconds)

      captor.getValue.key() shouldBe record.key()
      captor.getValue.value() shouldBe record.value()
      captor.getValue.topic() shouldBe topic

    }
  }
  override def beforeEach() = {
    super.beforeEach()
    reset(producer)
  }
}
