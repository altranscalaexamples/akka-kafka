package com.foo.bar.http

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.foo.bar.kafka.EventDispatcher
import com.foo.bar.model.error.ServiceError
import com.foo.bar.model.{error, LogEvent}
import org.scalatest.{Matchers, WordSpec}

class InputEventRouteSpec extends WordSpec with Matchers with ScalatestRouteTest {

  object TestRoute extends InputEventRoute {
    override val eventDispatcher: EventDispatcher = DummyEvent
  }

  import TestRoute._

  val correctInput = LogEvent(1L, "foo", 1L, None)
  val wrongInput   = LogEvent(0L, "foo", 1L, None)

  "InputEventRoute" should {

    "respond 201 on correct creation" in {

      Post("/api/events", correctInput) ~> Route.seal(eventRoutes) ~> check {
        status shouldBe StatusCodes.Created
      }

    }

    "respond 500 on service error" in {

      Post("/api/events", wrongInput) ~> Route.seal(eventRoutes) ~> check {
        status shouldBe StatusCodes.InternalServerError
      }

    }

  }

}

object DummyEvent extends EventDispatcher {
  override def sendMessage(event: LogEvent): Either[error.Error, Unit] = event.eventId match {
    case 1L => Right()
    case 0L => Left(ServiceError("BOOOOM!", None))
  }
}
