package com.foo.bar.kafka

import net.manub.embeddedkafka.{EmbeddedKafka, EmbeddedKafkaConfig}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import ProducerFactory._
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.foo.bar.model.{JsonCodec, KafkaRecord, LogEvent, TopicEvent}

import scala.concurrent.duration._

class KafkaServiceSpec
    extends TestKit(ActorSystem("system")) with WordSpecLike with Matchers with BeforeAndAfterAll with EmbeddedKafka with JsonCodec {

  implicit val config: EmbeddedKafkaConfig = EmbeddedKafkaConfig(kafkaPort = 9092, zooKeeperPort = 2181)

  val kafkaService = KafkaService(createProducer[TopicEvent])

  "KafkaService" should {

    "send message to kafka" in {
      withRunningKafka {
        val event  = LogEvent(1L, "foo", 1L, None)
        val tEvent = event.toTopicEvent
        kafkaService.sendMessage(event) shouldBe Right()
        receiveOne(10 seconds)

      }
    }

  }

}
