package com.foo.bar.model.error

trait Error                                                    extends Throwable
case class WrongStatusException(reason: String)                extends Error
case class ServiceError(reason: String, ex: Option[Throwable]) extends Error
