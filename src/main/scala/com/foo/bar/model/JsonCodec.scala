package com.foo.bar.model

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

trait JsonCodec extends SprayJsonSupport with DefaultJsonProtocol {

  implicit lazy val decoderOfLogEvent: RootJsonFormat[TopicEvent] = jsonFormat4(TopicEvent)
  implicit lazy val decoderOfInputEvent: RootJsonFormat[LogEvent] = jsonFormat4(LogEvent)

}
