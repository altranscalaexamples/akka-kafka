package com.foo.bar.model

case class TopicEvent(eventId: Long, userId: String, timestamp: Long, additionalInformation: Map[String, String])
case class LogEvent(eventId: Long, userId: String, timestamp: Long, additionalInformation: Option[Map[String, String]])
case class KafkaRecord[K, V](key: K, value: V)
