package com.foo.bar.kafka

import com.foo.bar.model.LogEvent
import com.foo.bar.model.error.Error

trait EventDispatcher {

  def sendMessage(event: LogEvent): Either[Error, Unit]

  def sendMessage(event: String): Either[Error, Unit] = Right(())

  def sendMessage(event: Int): Either[Error, Unit] = Right(())
}
