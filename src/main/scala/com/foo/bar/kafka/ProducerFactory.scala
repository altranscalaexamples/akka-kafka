package com.foo.bar.kafka

import java.util.Properties

import com.foo.bar.Config
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig}
import org.apache.kafka.common.serialization.StringSerializer
import spray.json.RootJsonFormat

object ProducerFactory {

  def createProducer[T]()(implicit format: RootJsonFormat[T]): KafkaProducer[String, T] = {
    val props = new Properties()
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, Config.kafkaBootstrapServer)
    props.put(ProducerConfig.RETRIES_CONFIG, "3")
    props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true")
    val importerSerializer = SerializerFactory[T]
    val stringSerializer   = new StringSerializer
    new KafkaProducer[String, T](props, stringSerializer, importerSerializer)
  }
}
