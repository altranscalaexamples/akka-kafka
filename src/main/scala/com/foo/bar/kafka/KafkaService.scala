package com.foo.bar.kafka

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import com.foo.bar.Config
import com.foo.bar.actor.ProducerActor
import com.foo.bar.model.error.{Error, ServiceError}
import com.foo.bar.model.{KafkaRecord, LogEvent, TopicEvent}
import org.apache.kafka.clients.producer.KafkaProducer

import scala.util.Try

class KafkaService(actor: ActorRef)(implicit actorSystem: ActorSystem) extends EventDispatcher {

  def sendMessage(event: LogEvent): Either[Error, Unit] = {
    val kafkaMessage: KafkaRecord[String, TopicEvent] = KafkaRecord(event.eventId.toString, event.toTopicEvent)
    Try(actor ! kafkaMessage).toEither.left.map(t => ServiceError(t.getMessage, Some(t)))
  }

}

object KafkaService {

  def apply(kafkaProducer: () => KafkaProducer[String, TopicEvent])(implicit actorSystem: ActorSystem): KafkaService = {
    lazy val topic = Config.kafkaOutputTopic
    lazy val actor = actorSystem.actorOf(Props.create(classOf[ProducerActor[String, TopicEvent]], kafkaProducer, topic), "kafkaProducer")
    new KafkaService(actor)
  }

}
