package com.foo.bar

import com.foo.bar.model.{LogEvent, TopicEvent}

package object kafka {

  implicit class EventOps(event: LogEvent) {

    def toTopicEvent: TopicEvent =
      TopicEvent(
        event.eventId,
        event.userId,
        event.timestamp,
        event.additionalInformation.getOrElse(Map.empty)
      )
  }

}
