package com.foo.bar.http

import com.typesafe.scalalogging.StrictLogging
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{complete, extractUri}
import akka.http.scaladsl.server.ExceptionHandler
import com.foo.bar.model.error.{ServiceError, WrongStatusException}

object RouteExceptionHandler extends StrictLogging {

  val exceptionHandler = ExceptionHandler {

    case e: WrongStatusException =>
      extractUri { uri =>
        logger.error(s"[$uri] : ${e.reason}")
        complete(StatusCodes.BadRequest -> e.reason)
      }
    case e: ServiceError =>
      extractUri { uri =>
        logger.error(s"[$uri] : ${e.reason}")
        complete(StatusCodes.InternalServerError -> e.reason)
      }
  }

}
