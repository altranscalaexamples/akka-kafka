package com.foo.bar.http

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import RouteExceptionHandler._
import com.foo.bar.kafka.EventDispatcher
import com.foo.bar.model.{JsonCodec, LogEvent}
import com.typesafe.scalalogging.StrictLogging

trait InputEventRoute extends StrictLogging with JsonCodec {

  val eventDispatcher: EventDispatcher

  val eventRoutes = pathPrefix("api") {
    postEventRoute
  }

  private val postEventRoute: Route =
    handleExceptions(exceptionHandler) {
      path("events") {
        postAndLog {
          entity(as[LogEvent]) { update: LogEvent =>
            complete {
              for {
                _ <- eventDispatcher.sendMessage(update)
              } yield StatusCodes.Created
            }
          }
        }
      }
    }

}
