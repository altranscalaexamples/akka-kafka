package com.foo.bar

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{extractRequest, mapResponse, path, post}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import com.typesafe.scalalogging.StrictLogging

package object http extends StrictLogging {

  def postAndLog(route: Route): Route =
    post(logRequestResponse(route))

  private def logRequestResponse =
    extractRequest.flatMap { request =>
      logger.info(s"$request")
      mapResponse { response =>
        logger.info(s"${response.entity}")
        response
      }
    }

  lazy val healthRoutes: Route =
    path("health") {
      logger.debug("I'm alive")
      complete(StatusCodes.OK)
    }

}
