package com.foo.bar

import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging

object Config extends StrictLogging {

  private val config = ConfigFactory.load()

  lazy val host: String = config.getString("http.host")
  lazy val port: Int    = config.getInt("http.port")

  lazy val kafkaBootstrapServer = config.getString("kafka.bootstrap-servers")
  lazy val kafkaOutputTopic     = config.getString("kafka.topics.log-output-topic")

}
