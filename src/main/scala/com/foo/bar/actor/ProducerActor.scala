package com.foo.bar.actor

import akka.actor.SupervisorStrategy.Resume
import akka.actor.{Actor, ActorLogging, OneForOneStrategy}
import com.foo.bar.model.{JsonCodec, KafkaRecord}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

import scala.concurrent.duration._

class ProducerActor[K, V](kafkaProducer: () => KafkaProducer[K, V], topic: String) extends Actor with ActorLogging with JsonCodec {

  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 minute) {
      case e: Throwable => {
        log.error(s"Error in ${context.self.path}: ${e.getMessage} Resuming.", e)
        Resume
      }
    }

  override def receive: Receive = {
    case message: KafkaRecord[K, V] => kafkaProducer().send(new ProducerRecord[K, V](topic, message.key, message.value))
  }

  override def preStart(): Unit = log.info(s"Created ${context.self.path}")
  override def postStop(): Unit = log.info("Exiting")

}
