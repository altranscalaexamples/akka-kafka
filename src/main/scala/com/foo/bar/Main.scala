package com.foo.bar

import akka.actor.{ActorSystem, Scheduler, Terminated}
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.ActorMaterializer
import com.foo.bar.http.Routes
import com.foo.bar.kafka.{KafkaService, ProducerFactory}
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.duration.{FiniteDuration, _}
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

object Main extends Routes with StrictLogging {

  protected val AkkaHttpGracefulShutdownTimeout: FiniteDuration = 5.seconds

  implicit val system: ActorSystem                = ActorSystem("exampleSystem")
  implicit val materializer: ActorMaterializer    = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher
  implicit val scheduler: Scheduler               = system.getScheduler

  val eventDispatcher = KafkaService(ProducerFactory.createProducer)

  def main(args: Array[String]): Unit = {
    val serverBinding = startHttpServer()
    addServerShutdownHook(serverBinding)
  }

  private def startHttpServer(): Future[ServerBinding] = {
    val serverBinding: Future[ServerBinding] = Http().bindAndHandle(routes, Config.host, Config.port)

    serverBinding.onComplete {
      case Success(bound) =>
        logger.info(s"Server online at http://${bound.localAddress.getHostString}:${bound.localAddress.getPort}/")
      case Failure(e) =>
        logger.error(s"Server could not start!")
        e.printStackTrace()
        system.terminate()
    }
    serverBinding
  }

  private def addServerShutdownHook(
      serverBinding: Future[ServerBinding]
  ): Unit =
    sys.addShutdownHook {
      val shutdownServer = serverBinding.flatMap(stopServerAndActorSystem)

      shutdownServer.onComplete {
        case Failure(error) =>
          logger.error(s"Application: http server stopped with an error!", error)
        case Success(_) =>
          logger.info(s"Application: http server stopped successfully.")
      }
      val shutdown = for {
        _ <- shutdownServer
      } yield ()

      Await.ready(shutdown, AkkaHttpGracefulShutdownTimeout)
      ()
    }

  private def stopServerAndActorSystem(serverBinding: ServerBinding): Future[Terminated] =
    serverBinding.unbind().flatMap { _ =>
      system.terminate()
    }

}
