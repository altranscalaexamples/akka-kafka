import sbt.Keys._
import sbt._
import Settings._
import Dependencies._

scalacOptions += "-Ypartial-unification"
scalacOptions += "-language:higherKinds"

lazy val stream = (project in file("."))
  .configs(IntegrationTest)
  .settings(name := "akka-kafka")
  .settings(libraryDependencies ++= dependencies)
  .defaultSettings
  .enablePlugins(JavaAppPackaging, DockerPlugin, AshScriptPlugin)

fork in run := true

mainClass in Compile := (mainClass in Compile in stream).value

inConfig(IntegrationTest)(org.scalafmt.sbt.ScalafmtPlugin.scalafmtConfigSettings)
