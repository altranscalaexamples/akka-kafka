import sbt._
object Dependencies {

  private[this] lazy val akkaVersion     = "2.5.21"
  private[this] lazy val akkaHttpVersion = "10.1.7"
  private lazy val akkaDependencies = Seq(
    "com.typesafe.akka"          %% "akka-http"            % akkaHttpVersion,
    "com.typesafe.akka"          %% "akka-http-spray-json" % akkaHttpVersion,
    "com.typesafe.akka"          %% "akka-stream"          % akkaVersion,
    "com.typesafe.scala-logging" %% "scala-logging"        % "3.9.0"
  )

  private[this] lazy val slf4jVersion = "1.7.25"
  private lazy val logDependencies = Seq(
    "org.slf4j" % "slf4j-api"     % slf4jVersion,
    "org.slf4j" % "slf4j-log4j12" % slf4jVersion,
    "log4j"     % "log4j"         % "1.2.17"
  )

  private lazy val kafkaDependencies = Seq(
    "org.apache.kafka" % "kafka-clients"   % "2.0.0",
    "javax.ws.rs"      % "javax.ws.rs-api" % "2.1.1" artifacts Artifact("javax.ws.rs-api", "jar", "jar")
  )

  private[this] lazy val versionScalaTest = "3.0.5"
  private lazy val testDependencies = Seq(
    "io.github.embeddedkafka" %% "embedded-kafka"    % "2.3.1"          % "it, test",
    "com.typesafe.akka"       %% "akka-testkit"      % akkaVersion      % "it, test",
    "com.typesafe.akka"       %% "akka-http-testkit" % akkaHttpVersion  % Test,
    "org.scalatest"           %% "scalatest"         % versionScalaTest % "it, test",
    "org.mockito"             % "mockito-all"        % "1.10.19"        % "it, test"
  )

  val dependencies: Seq[ModuleID] = akkaDependencies
    .++(logDependencies)
    .++(kafkaDependencies)
    .++(testDependencies)

}
